#ifndef NODE_H
#define NODE_H

#include <cstdlib>

class Node{
    friend class Queue;
public:
    Node(){info=-1;next=NULL;}
    Node* getNext(){return next;}
    int getInfo(){return info;}
    void setNext(Node* n){next=n;}
    void setInfo(int i){info=i;}
private:
    int info;
    Node* next;
};

#endif