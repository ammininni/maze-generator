# makefile

# define target, its dependencies and file
project: main.o queue.o
	g++ -o project main.o queue.o

# define each object
main.o: main.cpp queue.h
	g++ -c main.cpp

queue.o: queue.cpp queue.h node.h
	g++ -c queue.cpp

# clean up
clean:
	rm -f project *.o *~
