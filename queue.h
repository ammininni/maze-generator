#ifndef QUEUE_H
#define QUEUE_H

#include <iostream>

#include "Node.h"

class Queue{
public:
    Queue(){first=NULL;}
    ~Queue();
    void enqueue(int);
    int dequeue();
    bool isEmpty(){return first==NULL;}
    void printQueue();
private:
    Node* first;
    Node* last;
};

#endif