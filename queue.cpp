#include "queue.h"

Queue::~Queue(){
    while(last){
        dequeue();
    }
}

void Queue::enqueue(int i){
    Node* temp = new Node();
    temp->setInfo(i);
    if(first){
        last->setNext(temp);
    } else {
        first = temp;
    }
    last = temp;
}

int Queue::dequeue(){
    if(first){
        int temp = first->getInfo();
        first= first->getNext();
        if(first== NULL){
            last = NULL;
        }
        return temp;
    }
    return -1;
}

void Queue::printQueue(){
    Node* temp=first;
    while(temp){
        std::cout << temp->getInfo() << " -> ";
        temp = temp->getNext();
    }
    std::cout << "NULL";
}